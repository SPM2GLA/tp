#!/bin/bash
cd hsdbserver
export PATH=$PATH:`pwd`

if pgrep -f "hsqldb" > /dev/null
then
    echo "Running"


echo 
echo 
echo curl -H Content-Type: application/json -X GET http://localhost:8080/create/{email}/{name}
sleep 1
echo 
curl -H 'Content-Type: application/json' -X GET http://localhost:8080/create/toto@mymail.ru/toto
echo 
curl -H 'Content-Type: application/json' -X GET http://localhost:8080/create/nana@mail.ru/Nana
echo 
curl -H 'Content-Type: application/json' -X GET http://localhost:8080/create/xxxxxxxxxx@xxxx.co.jp/Cantreadthat
echo 
echo 
echo curl -H Content-Type: application/json -X GET http://localhost:8080/get-by-email/{email}/
echo 
sleep 1
echo 
curl -H 'Content-Type: application/json' -X GET http://localhost:8080/get-by-email/nana@mail.ru/
echo 
curl -H 'Content-Type: application/json' -X GET http://localhost:8080/get-by-email/xxxxxxxxxx@xxxx.co.jp/
echo 
echo 
echo curl -H 'Content-Type: application/json' -X GET http://localhost:8080/delete/{id}
echo 
sleep 1
echo 
curl -H 'Content-Type: application/json' -X GET http://localhost:8080/delete/3
echo 
echo curl -H 'Content-Type: application/json' -X GET http://localhost:8080/update/{id}/{email}/{name}
echo 
echo 
sleep 1
echo 
curl -H 'Content-Type: application/json' -X GET http://localhost:8080/update/2/nana@nian.co.jp/nana
echo 


else
    echo "Starting"
	f [ ! -d "data" ]; mkdir data
	xterm -e ./run-hsqldb-server.sh&
	sleep 6
	echo "Now restart the java project and do it again"
fi

