package sample.data.jpa.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Account {
	
	private long id;
	private int datecreation;
	private User user;
	public Account(int datecreation, User user) {
		super();
		this.datecreation = datecreation;
		this.user = user;
	}
	public Account() {
		super();
		
	}
	public Account(int datecreation) {
		super();
		this.datecreation = datecreation;
		
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getDatecreation() {
		return datecreation;
	}
	public void setDatecreation(int datecreation) {
		this.datecreation = datecreation;
	}
	@ManyToOne
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "Account [id=" + id + ", datecreation=" + datecreation + ", user=" + user + "]";
	}
	
	
	
	

}
