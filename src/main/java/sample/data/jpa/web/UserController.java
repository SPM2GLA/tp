package sample.data.jpa.web;



import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import sample.data.jpa.domain.Account;
import sample.data.jpa.domain.User;
import sample.data.jpa.service.AccountDao;
import sample.data.jpa.service.UserDao;

@Controller
public class UserController {
	@Autowired
	private UserDao userDao;
	@Autowired
	private AccountDao aDao;

	/**
	 * GET /create  --> Create a new user and save it in the database.
	 */
	@RequestMapping("/create/{email}/{name}")
	@ResponseBody
	public String create(@PathVariable("email")String email,@PathVariable("name")String name) {



		String userId = "";
		System.err.println("adding "+email+" "+name);
		try {
			//test si existe
			User user = userDao.findByEmail(email);
			if (user != null){
				userId = String.valueOf(user.getId());
				return "User already present with id = " + userId;
			}
			else {
				User user2 = new User(email, name);
				userDao.save(user2);
				userId = String.valueOf(user2.getId());

			}
		}
		catch (Exception ex) {
			return "Error creating the user: " + ex.toString();
		}
		return "User succesfully created with id = " + userId;
	}
	@RequestMapping("/createaccount/{date}/{userid}")
	@ResponseBody
	public String createAccount(@PathVariable("date")int date,@PathVariable("userid") long id) {
		String accountId = "";
		System.err.println(""+date+"  "+id);
		try {
			System.err.println("1");
			Account a = new Account(date,userDao.findById(id));
			//			a.setUser(userDao.findById(id));
			System.err.println("2");
			aDao.save(a);
			System.err.println("3");
			System.err.println(a);
			List b = new ArrayList<Account>();
			b.add(a);
			a.getUser().setAccount(b);


			accountId = String.valueOf(a.getId());
		}
		catch (Exception ex) {
			return "Error creating the user: " + ex.toString();
		}
		return "User succesfully created with id = " + accountId;
	}


	/**
	 * GET /delete  --> Delete the user having the passed id.
	 */
	@RequestMapping("/delete/{id}")
	@ResponseBody
	public String delete(@PathVariable("id")long id) {

		try {
			User user = userDao.findOne(id);
			if (user != null){
				userDao.delete(user);
			}
			else
				return "Error deleting the user id "+ id + " not found";
		}
		catch (Exception ex) {
			return "Error deleting the user:" + ex.toString();
		}
		return "User succesfully deleted!";
	}

	/**
	 * GET /get-by-email  --> Return the id for the user having the passed
	 * email.
	 */

	@RequestMapping("/get-by-email/{email}")
	@ResponseBody
	public String getByEmail(@PathVariable("email")String email) {

		String userId = "";
		try {
			System.err.println("looking for user "+email);
			User user = userDao.findByEmail(email);
			userId = String.valueOf(user.getId());
		}
		catch (Exception ex) {
			return "User not found";
		}
		return "The user id is: " + userId;
	}

	/**
	 * GET /update  --> Update the email and the name for the user in the 
	 * database having the passed id.
	 */
	@RequestMapping("/update/{id}/{email}/{name}")
	@ResponseBody
	public String updateUser(@PathVariable long id, @PathVariable String email, @PathVariable String name) {
		try {
			User user = userDao.findOne(id);
			user.setEmail(email);
			user.setName(name);
			userDao.save(user);
		}
		catch (Exception ex) {
			return "Error updating the user: " + ex.toString();
		}
		return "User succesfully updated!";
	}

	// Private fields



}